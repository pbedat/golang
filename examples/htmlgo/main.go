package main

import (
	"html/template"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/theplant/htmlgo"
	"gitlab.com/pbedat/golang/web/comp"
)

func main() {

	r := mux.NewRouter()

	r.Path("/").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		tpl := template.Must(template.New("main").Parse(`<html>
		<body>
		  <h1>Hello</h1>
		  {{ call .div }}
		 </body>
		 </html> `))

		div := htmlgo.Div().Text("component")

		tpl.Execute(w, map[string]any{
			"comp": comp.AsPartial(div, r.Context()),
		})
	})

	http.ListenAndServe(":8080", r)
}
