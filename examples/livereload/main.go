package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/fsnotify/fsnotify"
	"gitlab.com/pbedat/golang/web"
)

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "text/html")
		w.WriteHeader(200)

		fmt.Fprintf(w, `
		<html>
		  <head>%s</head>
		  <body>
		    <h1>Edit this HTML!</h1>
			<a href="/wat">wat?</a>
		  </body>
		</html>
		`, web.InjectLivereloadHtml(""))
	})

	livereload := web.NewLivereloadMiddleware("")

	s := http.Server{
		Addr:    ":8080",
		Handler: livereload.Handler(http.DefaultServeMux),
	}

	s.RegisterOnShutdown(func() {
		livereload.Close()
	})

	go s.ListenAndServe()
	log.Println("listening to :8080...")

	go func() {
		w, _ := fsnotify.NewWatcher()
		w.Add("main.go")

		for {
			<-w.Events
			log.Println("triggy")
			livereload.Trigger("/")
		}
	}()

	cSig := make(chan os.Signal, 1)
	signal.Notify(cSig, syscall.SIGTERM, syscall.SIGINT)

	<-cSig

	log.Println("bye")
	log.Println(s.Shutdown(context.Background()))

}
