module gitlab.com/pbedat/golang

go 1.19

require (
	github.com/bep/godartsass v0.14.0
	github.com/evanw/esbuild v0.15.9
	github.com/fsnotify/fsnotify v1.6.0
	github.com/google/go-cmp v0.5.9
	github.com/google/uuid v1.3.0
	github.com/gorilla/mux v1.8.0
	github.com/gregjones/httpcache v0.0.0-20190611155906-901d90724c79
	github.com/rs/zerolog v1.28.0
	github.com/samber/lo v1.35.0
	github.com/theplant/htmlgo v1.0.3
	golang.org/x/crypto v0.1.0
)

require (
	github.com/cli/safeexec v1.0.0 // indirect
	github.com/frankban/quicktest v1.14.3 // indirect
	github.com/google/btree v1.1.2 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.16 // indirect
	github.com/peterbourgon/diskv v2.0.1+incompatible // indirect
	github.com/rogpeppe/go-internal v1.8.0 // indirect
	github.com/stretchr/testify v1.8.1 // indirect
	golang.org/x/exp v0.0.0-20221114191408-850992195362 // indirect
	golang.org/x/sys v0.2.0 // indirect
	golang.org/x/term v0.2.0 // indirect
	google.golang.org/protobuf v1.28.1 // indirect
)
