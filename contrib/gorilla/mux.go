package gorilla

import (
	"context"
	"fmt"
	"net/http"
	"net/url"

	"github.com/gorilla/mux"
)

type UrlHelper struct {
	router  *mux.Router
	context context.Context
	vars    []string
	query   url.Values
}

// helper for route urls
func Url(ctx context.Context) *UrlHelper {
	vars := mux.Vars(GetRequest(ctx))
	flatVars := make([]string, len(vars)*2)
	i := 0
	for key, val := range vars {
		flatVars[i] = key
		flatVars[i+1] = val
		i += 2
	}
	return &UrlHelper{GetRouter(ctx), ctx, flatVars, nil}
}

func (h *UrlHelper) Vars(vars ...interface{}) *UrlHelper {
	if len(vars) == 0 {
		return h
	}
	stringVars := make([]string, len(vars))
	for i, v := range vars {
		stringVars[i] = fmt.Sprint(v)
	}
	h.vars = append(h.vars, stringVars...)
	return h
}

func (h *UrlHelper) Query(values url.Values) *UrlHelper {
	h.query = values
	return h
}

func (h *UrlHelper) Href(route string) string {
	r := h.router.Get(route)

	if r == nil {
		panic(fmt.Errorf("route '%s' not found", route))
	}

	u, err := r.URL(h.vars...)

	if err != nil {
		panic(fmt.Errorf("route '%s' not found: %w", route, err))
	}

	if u != nil {
		u.RawQuery = h.query.Encode()
	}

	return u.String()
}

func (h *UrlHelper) IsActive(route string) bool {
	return mux.CurrentRoute(GetRequest(h.context)).GetName() == route
}

type routerKey struct{}
type requestKey struct{}

func GetRequest(ctx context.Context) *http.Request {
	r := ctx.Value(requestKey{})

	if r == nil {
		panic("could not retrieve request from context")
	}

	return r.(*http.Request)
}

func GetRouter(ctx context.Context) *mux.Router {
	router := ctx.Value(routerKey{})

	if router == nil {
		panic("could not retrieve router from context")
	}

	return router.(*mux.Router)
}

func MuxContextMiddleware(router *mux.Router) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
			ctx := context.WithValue(r.Context(), routerKey{}, router)
			ctx = context.WithValue(ctx, requestKey{}, r)
			next.ServeHTTP(rw, r.WithContext(ctx))
		})
	}
}
