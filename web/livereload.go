package web

import (
	_ "embed"
	"fmt"
	"log"
	"net/http"
	"sync"
	"text/template"
	"time"

	"github.com/google/uuid"
)

//go:embed livereload.js
var script string

const publicScriptPath = "/_livereload/livereload.js"

func InjectLivereloadHtml(baseUrl string) string {
	return fmt.Sprintf(`<script type="module" src="%s%s"></script>`, baseUrl, publicScriptPath)
}

type liveReloadMiddleware struct {
	sse *sseHandler
	scriptHandler
}

func (h *liveReloadMiddleware) Handler(next http.Handler) http.Handler {

	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		switch r.URL.Path {
		case publicScriptPath:
			h.scriptHandler.ServeHTTP(rw, r)
		case "/_livereload/events":
			h.sse.ServeHTTP(rw, r)
		default:
			next.ServeHTTP(rw, r)
		}
	})
}

func (h *liveReloadMiddleware) Close() {
	h.sse.Close()
}

func NewLivereloadMiddleware(baseUrl string) *liveReloadMiddleware {
	wg := &sync.WaitGroup{}
	wg.Add(1)
	return &liveReloadMiddleware{
		sse: &sseHandler{
			wgClose:  wg,
			triggers: make(map[string]chan string),
		},
		scriptHandler: scriptHandler{
			baseUrl: baseUrl,
		},
	}
}

func (h *liveReloadMiddleware) Trigger(path string) {
	for _, t := range h.sse.triggers {
		select {
		case t <- path:
		default:
			log.Println("channel overflow")
		}
	}
}

var scriptTpl = template.Must(template.New("script").Parse(script))

type scriptHandler struct {
	baseUrl string
}

func (h scriptHandler) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	rw.Header().Add("Content-Type", "text/javascript")
	rw.WriteHeader(200)

	err := scriptTpl.Execute(rw, map[string]string{"baseUrl": h.baseUrl})
	if err != nil {
		panic(err)
	}
}

type sseHandler struct {
	wgClose  *sync.WaitGroup
	triggers map[string]chan string
	sync.Mutex
}

func (h *sseHandler) Close() {
	for _, t := range h.triggers {
		close(t)
	}
	h.wgClose.Done()
}

func (h *sseHandler) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	rw.Header().Add("Cache-Control", "no-store")
	rw.Header().Add("Content-Type", "text/event-stream")
	rw.WriteHeader(200)

	fmt.Fprintln(rw, "data: hello")
	fmt.Fprintln(rw)

	rw.(http.Flusher).Flush()

	t := time.NewTimer(10 * time.Second)

	cClose := make(chan bool, 1)

	go func() {
		h.wgClose.Wait()
		cClose <- true
	}()

	id := uuid.NewString()

	trigger := make(chan string, 1)
	defer close(trigger)

	h.Lock()
	h.triggers[id] = trigger
	h.Unlock()
	defer func() {
		h.Lock()
		defer h.Unlock()
		delete(h.triggers, id)
	}()

	for {

		select {
		case p := <-trigger:
			fmt.Fprintln(rw, "event: change")
			fmt.Fprintln(rw, "data:", p)
			fmt.Fprintln(rw)
			rw.(http.Flusher).Flush()
		case <-cClose:
			fmt.Fprintln(rw, "event: shutdown")
			fmt.Fprintln(rw, "data:", time.Now())
			fmt.Fprintln(rw, "retry: 500")
			fmt.Fprintln(rw)
			rw.(http.Flusher).Flush()
			return
		case <-r.Context().Done():
			fmt.Fprintln(rw, "event: shutdown")
			fmt.Fprintln(rw, "data:", time.Now())
			fmt.Fprintln(rw, "retry: 500")
			fmt.Fprintln(rw)
			rw.(http.Flusher).Flush()
			return
		case <-t.C:
			fmt.Fprintln(rw, "event: ping")
			fmt.Fprintln(rw, "data:", time.Now())
			fmt.Fprintln(rw)

			rw.(http.Flusher).Flush()
		}

		t.Reset(10 * time.Second)
	}

}
