package comp

import (
	"net/http"

	"github.com/theplant/htmlgo"
)

func Handler(comp htmlgo.HTMLComponent) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		Render(w, r, comp)
	})
}

func Render(w http.ResponseWriter, r *http.Request, comp htmlgo.HTMLComponent) {
	w.Header().Add("Content-Type", "text/html")
	w.WriteHeader(200)
	htmlgo.Fprint(w, comp, r.Context())
}
