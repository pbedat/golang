package comp

import (
	"bytes"
	"context"
	"html/template"

	"github.com/rs/zerolog/log"
	"github.com/theplant/htmlgo"
	"gitlab.com/pbedat/golang/lang"
)

// Makes a htmlgo.HTMLComponent callable in a html/template
func AsPartial(comp htmlgo.HTMLComponent, ctx context.Context) PartialComp {
	return func() (template.HTML, error) {
		b, err := comp.MarshalHTML(ctx)
		if err != nil {
			return template.HTML(""), err
		} else {
			return template.HTML(string(b)), nil
		}
	}
}

type PartialComp func() (template.HTML, error)

// Wraps a go template in a HTMLComponent
func FromTemplate(name string, tpl string, data interface{}) htmlgo.HTMLComponent {

	t := lang.Must(template.New(name).Parse(tpl))

	return htmlgo.ComponentFunc(func(ctx context.Context) (r []byte, err error) {
		b := bytes.NewBuffer(make([]byte, len(tpl)))

		if err := t.Execute(b, data); err != nil {
			log.Error().Err(err).Send()
			return nil, err
		}

		return b.Bytes(), nil
	})
}
