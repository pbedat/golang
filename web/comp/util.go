package comp

import "github.com/theplant/htmlgo"

func Fragment(children ...htmlgo.HTMLComponent) htmlgo.HTMLComponents {
	return htmlgo.Components(children...)
}
