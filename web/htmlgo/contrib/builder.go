package htmlgo_contrib

import (
	"context"

	"github.com/theplant/htmlgo"
)

func NewGenericBuilder[T any](parent *T, tag *htmlgo.HTMLTagBuilder) *GenericBuilder[T] {
	return &GenericBuilder[T]{parent, tag}
}

type GenericBuilder[T any] struct {
	parent *T
	tag    *htmlgo.HTMLTagBuilder
}

func (gb *GenericBuilder[T]) MarshalHTML(ctx context.Context) ([]byte, error) {
	return gb.tag.MarshalHTML(ctx)
}

func (gb *GenericBuilder[T]) Tag() *htmlgo.HTMLTagBuilder {
	return gb.tag
}

func (gb *GenericBuilder[T]) As(as *htmlgo.HTMLTagBuilder) *GenericBuilder[T] {
	gb.tag = as

	return gb
}

func (gb *GenericBuilder[T]) Children(children ...htmlgo.HTMLComponent) *T {
	gb.tag.Children(children...)
	return gb.parent
}

func (gb *GenericBuilder[T]) Action(action string) *T {
	gb.tag.Action(action)
	return gb.parent
}

func (gb *GenericBuilder[T]) Text(text string) *T {
	gb.tag.Text(text)
	return gb.parent
}

func (gb *GenericBuilder[T]) Src(src string) *T {
	gb.tag.Src(src)
	return gb.parent
}

func (gb *GenericBuilder[T]) OmitEndTag() *T {
	gb.tag.OmitEndTag()
	return gb.parent
}

func (gb *GenericBuilder[T]) Data(keyValuePairs ...string) *T {
	gb.tag.Data(keyValuePairs...)
	return gb.parent
}

func (gb *GenericBuilder[T]) Attr(keyValuePairs ...interface{}) *T {
	gb.tag.Attr(keyValuePairs...)
	return gb.parent
}

func (gb *GenericBuilder[T]) Type(t string) *T {
	gb.tag.Type(t)
	return gb.parent
}

func (gb *GenericBuilder[T]) Href(href string) *T {
	gb.tag.Href(href)
	return gb.parent
}

func (gb *GenericBuilder[T]) Class(names ...string) *T {
	gb.tag.Class(names...)
	return gb.parent
}

func (gb *GenericBuilder[T]) Target(target string) *T {
	gb.tag.Target(target)
	return gb.parent
}
