package bootstrap

import (
	"context"
	"fmt"

	"github.com/theplant/htmlgo"
	htmlgo_contrib "gitlab.com/pbedat/golang/web/htmlgo/contrib"
)

type BtnVariant string

const (
	BtnVariantPrimary   BtnVariant = "primary"
	BtnVariantSecondary BtnVariant = "secondary"
	BtnVariantSuccess   BtnVariant = "success"
	BtnVariantWarn      BtnVariant = "warn"
	BtnVariantDanger    BtnVariant = "danger"
	BtnVariantLight     BtnVariant = "light"
	BtnVariantLink      BtnVariant = "link"
	BtnVariantDark      BtnVariant = "dark"
)

type buttonSize string

const (
	BtnSizeSm buttonSize = "sm"
	BtnSizeMd buttonSize = "md"
	BtnSizeLg buttonSize = "lg"
)

type buttonBuilder struct {
	tag      *htmlgo.HTMLTagBuilder
	variant  BtnVariant
	text     string
	size     buttonSize
	outline  bool
	disabled bool
	toggle   bool
	active   bool

	*htmlgo_contrib.GenericBuilder[buttonBuilder]
}

func Button(text string) *buttonBuilder {
	b := buttonBuilder{
		tag:      htmlgo.Button(text),
		text:     text,
		variant:  BtnVariantPrimary,
		size:     BtnSizeMd,
		outline:  false,
		disabled: false,
		active:   false,
	}

	b.GenericBuilder = htmlgo_contrib.NewGenericBuilder(&b, b.tag)

	return &b
}

func (props *buttonBuilder) Variant(variant BtnVariant) *buttonBuilder {

	props.variant = variant
	return props
}

func (props *buttonBuilder) Size(size buttonSize) *buttonBuilder {
	props.size = size
	return props
}

func (props *buttonBuilder) Outline(outline ...bool) *buttonBuilder {
	if len(outline) == 0 {
		props.outline = true
	} else {
		props.outline = outline[0]
	}

	return props
}

func (props *buttonBuilder) Disabled(disabled ...bool) *buttonBuilder {
	if len(disabled) == 0 {
		props.disabled = true
	} else {
		props.disabled = disabled[0]
	}
	return props
}

func (props *buttonBuilder) Toggle(active bool) *buttonBuilder {
	props.toggle = true
	props.active = active
	return props
}

func (props *buttonBuilder) As(as *htmlgo.HTMLTagBuilder) *buttonBuilder {
	props.tag = as
	props.GenericBuilder.As(as)
	return props
}

func (props *buttonBuilder) MarshalHTML(ctx context.Context) ([]byte, error) {
	// TODO should check the tag of props.as and add a role="button" attr if its not a button
	// but htmlgo does not export the tag property
	btn := props.tag.Class("btn")

	if props.text != "" {
		btn.Text(props.text)
	}

	if props.outline {
		props.variant = "outline-" + props.variant
	}

	btn.Class(string("btn-" + props.variant))

	if props.size != BtnSizeMd {
		btn.Class("btn-" + string(props.size))
	}

	if props.toggle {
		btn.Data("bs-toggle", "button").
			ClassIf("active", props.active).
			Attr("aria-pressed", fmt.Sprint(props.active))
	}

	if props.disabled {
		btn.Disabled(props.disabled)
	}

	return btn.MarshalHTML(ctx)
}
