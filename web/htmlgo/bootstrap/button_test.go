package bootstrap

import (
	"context"
	"strings"
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/theplant/htmlgo"
)

func TestButton(t *testing.T) {
	t.Run("defaults", func(t *testing.T) {
		buf, err := Button("Test").MarshalHTML(context.TODO())

		if err != nil {
			t.Error(err)
		}

		got := strings.Trim(string(buf), "\n")
		wanted := `<button class='btn btn-primary'>Test</button>`

		if !cmp.Equal(got, wanted) {
			t.Errorf("got %s, but wanted %s:\n%s", got, wanted, cmp.Diff(got, wanted))
		}
	})

	t.Run("variant", func(t *testing.T) {
		buf, err := Button("Test").Variant("danger").MarshalHTML(context.TODO())

		if err != nil {
			t.Error(err)
		}

		got := strings.Trim(string(buf), "\n")
		wanted := `<button class='btn btn-danger'>Test</button>`

		if !cmp.Equal(got, wanted) {
			t.Errorf("got %s, but wanted %s:\n%s", got, wanted, cmp.Diff(got, wanted))
		}
	})

	t.Run("variant", func(t *testing.T) {
		buf, err := Button("Test").Variant(BtnVariantWarn).Outline().MarshalHTML(context.TODO())

		if err != nil {
			t.Error(err)
		}

		got := strings.Trim(string(buf), "\n")
		wanted := `<button class='btn btn-outline-warn'>Test</button>`

		if !cmp.Equal(got, wanted) {
			t.Errorf("got %s, but wanted %s:\n%s", got, wanted, cmp.Diff(got, wanted))
		}
	})

	t.Run("disabled", func(t *testing.T) {
		buf, err := Button("Test").Disabled().MarshalHTML(context.TODO())

		if err != nil {
			t.Error(err)
		}

		got := strings.Trim(string(buf), "\n")
		wanted := `<button disabled class='btn btn-primary'>Test</button>`

		if !cmp.Equal(got, wanted) {
			t.Errorf("got %s, but wanted %s:\n%s", got, wanted, cmp.Diff(got, wanted))
		}
	})

	t.Run("toggle false", func(t *testing.T) {
		buf, err := Button("Test").Toggle(false).MarshalHTML(context.TODO())

		if err != nil {
			t.Error(err)
		}

		got := strings.Trim(string(buf), "\n")
		wanted := `<button data-bs-toggle='button' aria-pressed='false' class='btn btn-primary'>Test</button>`

		if !cmp.Equal(got, wanted) {
			t.Errorf("got %s, but wanted %s:\n%s", got, wanted, cmp.Diff(got, wanted))
		}
	})

	t.Run("toggle true", func(t *testing.T) {
		buf, err := Button("Test").Toggle(true).MarshalHTML(context.TODO())

		if err != nil {
			t.Error(err)
		}

		got := strings.Trim(string(buf), "\n")
		wanted := `<button data-bs-toggle='button' aria-pressed='true' class='btn btn-primary active'>Test</button>`

		if !cmp.Equal(got, wanted) {
			t.Errorf("got %s, but wanted %s:\n%s", got, wanted, cmp.Diff(got, wanted))
		}
	})

	t.Run("as", func(t *testing.T) {
		buf, err := Button("Test").As(
			htmlgo.A(),
		).Href("test").Attr("role", "button").MarshalHTML(context.TODO())

		if err != nil {
			t.Error(err)
		}

		got := strings.Trim(string(buf), "\n")
		wanted := `<a href='test' role='button' class='btn btn-primary'>Test</a>`

		if !cmp.Equal(got, wanted) {
			t.Errorf("got %s, but wanted %s:\n%s", got, wanted, cmp.Diff(got, wanted))
		}
	})

	t.Run("with children", func(t *testing.T) {
		btn := Button("").Variant(BtnVariantPrimary).Children(htmlgo.I("").Class("fa fa-check"))
		buf, err := btn.MarshalHTML(context.TODO())
		if err != nil {
			t.Error(err)
		}

		got := strings.ReplaceAll(string(buf), "\n", "")
		wanted := "<button class='btn btn-primary'><i class='fa fa-check'></i></button>"

		if !cmp.Equal(got, wanted) {
			t.Errorf("got %s, but wanted %s:\n%s", got, wanted, cmp.Diff(got, wanted))
		}
	})
}
