package bootstrap

import (
	"context"

	"github.com/theplant/htmlgo"
	htmlgo_contrib "gitlab.com/pbedat/golang/web/htmlgo/contrib"
)

type bsVariant string

const (
	BsVariantPrimary   bsVariant = "primary"
	BsVariantSecondary bsVariant = "secondary"
	BsVariantSuccess   bsVariant = "success"
	BsVariantInfo      bsVariant = "info"
	BsVariantWarn      bsVariant = "warn"
	BsVariantDanger    bsVariant = "danger"
	BsVariantLight     bsVariant = "light"
	BsVariantDark      bsVariant = "dark"
)

type alertBuilder struct {
	*htmlgo_contrib.GenericBuilder[alertBuilder]
	variant bsVariant
}

func Alert(children ...htmlgo.HTMLComponent) *alertBuilder {
	b := &alertBuilder{
		variant: BsVariantPrimary,
	}

	b.GenericBuilder = htmlgo_contrib.NewGenericBuilder(b, htmlgo.Div().Role("alert"))

	return b
}

func (alert *alertBuilder) Variant(variant bsVariant) *alertBuilder {
	alert.variant = variant
	return alert
}

func (alert *alertBuilder) MarshalHTML(ctx context.Context) ([]byte, error) {
	return alert.GenericBuilder.Class("alert alert-" + string(alert.variant)).Tag().MarshalHTML(ctx)
}

func AlertLink(builder htmlgo.HTMLTagBuilder) *htmlgo.HTMLTagBuilder {
	return builder.Class("alert-link")
}

func AlertHeading(builder htmlgo.HTMLTagBuilder) *htmlgo.HTMLTagBuilder {
	return builder.Class("alert-heading")
}

func AlertCloseButton() htmlgo.HTMLTagBuilder {
	return *htmlgo.Button("").Class("btn-close").
		Data("bs-dismiss", "alert").Attr("aria-label", "Close")
}
