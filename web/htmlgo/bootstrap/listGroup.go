package bootstrap

import (
	"github.com/theplant/htmlgo"
)

type listGroupProps struct {
	*htmlgo.HTMLTagBuilder
}

func ListGroup() listGroupProps {
	var props listGroupProps
	return props.As(htmlgo.Ul())
}

func (props listGroupProps) As(builder *htmlgo.HTMLTagBuilder) listGroupProps {
	props.HTMLTagBuilder = builder.Class("list-group")
	return props
}

func (props listGroupProps) Flush(flush ...bool) listGroupProps {
	_flush := len(flush) == 0

	if !_flush {
		_flush = flush[0]
	}

	props.HTMLTagBuilder = props.ClassIf("list-group-flush", _flush)
	return props
}

func (props listGroupProps) Numbered(numbered ...bool) listGroupProps {
	_numbered := len(numbered) == 0

	if !_numbered {
		_numbered = numbered[0]
	}

	props.HTMLTagBuilder = props.ClassIf("list-group-numbered", _numbered)
	return props
}

type listGroupItemBuilder struct {
	*htmlgo.HTMLTagBuilder
}

func ListGroupItem(children ...htmlgo.HTMLComponent) *listGroupItemBuilder {
	b := &listGroupItemBuilder{}

	b.As(htmlgo.A(children...))

	return b
}

func (item *listGroupItemBuilder) As(builder *htmlgo.HTMLTagBuilder) *listGroupItemBuilder {
	item.HTMLTagBuilder = builder.Class("list-group-item")
	return item
}

func (item *listGroupItemBuilder) Active(active bool) *listGroupItemBuilder {
	item.ClassIf("active", active)

	return item
}

func (item *listGroupItemBuilder) Disabled(disabled bool) *listGroupItemBuilder {
	item.ClassIf("disabled", disabled)

	return item
}

func (item *listGroupItemBuilder) Action(disabled ...bool) *listGroupItemBuilder {
	_disabled := len(disabled) == 0

	if !_disabled {
		_disabled = disabled[0]
	}

	item.ClassIf("list-group-item-action", _disabled)

	return item
}

func (item *listGroupItemBuilder) Variant(variant bsVariant) *listGroupItemBuilder {
	item.Class("list-group-item-" + string(variant))

	return item
}
