package bootstrap

import (
	"context"
	"strings"
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestAlert(t *testing.T) {
	t.Run("defaults", func(t *testing.T) {
		buf, err := Alert().Text("test").MarshalHTML(context.TODO())

		if err != nil {
			t.Error(err)
		}

		got := strings.Trim(string(buf), "\n")
		wanted := `<div role='alert' class='alert alert-primary'>test</div>`

		if !cmp.Equal(got, wanted) {
			t.Errorf("got %s, but wanted %s:\n%s", got, wanted, cmp.Diff(got, wanted))
		}
	})

	t.Run("variant", func(t *testing.T) {
		buf, err := Alert().Variant(BsVariantInfo).Text("test").MarshalHTML(context.TODO())

		if err != nil {
			t.Error(err)
		}

		got := strings.Trim(string(buf), "\n")
		wanted := `<div role='alert' class='alert alert-info'>test</div>`

		if !cmp.Equal(got, wanted) {
			t.Errorf("got %s, but wanted %s:\n%s", got, wanted, cmp.Diff(got, wanted))
		}
	})
}
