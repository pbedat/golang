package turbo

import (
	"context"
	"net/http"

	"github.com/theplant/htmlgo"
	. "github.com/theplant/htmlgo"
	htmlgo_contrib "gitlab.com/pbedat/golang/web/htmlgo/contrib"
)

type frameBuilder struct {
	*htmlgo_contrib.GenericBuilder[frameBuilder]
}

func Frame(id string) *frameBuilder {
	b := frameBuilder{}
	b.GenericBuilder = htmlgo_contrib.NewGenericBuilder(&b, Tag("turbo-frame").Id(id))
	return &b
}

func (tf *frameBuilder) Lazy(lazy bool) *frameBuilder {
	tf.GenericBuilder.Attr("loading", "lazy")
	return tf
}

func (tf *frameBuilder) Action(action TurboVisitAction) *frameBuilder {
	tf.Tag().Data("turbo-action", string(action))
	return tf
}

type TurboVisitAction string

const (
	TurboVisitAdvance TurboVisitAction = "advance"
	TurboVisitReplace TurboVisitAction = "replace"
)

var FormatTurboStream = "text/vnd.turbo-stream.html"

func Stream() *streamBuilder {
	b := streamBuilder{}
	b.GenericBuilder = htmlgo_contrib.NewGenericBuilder(&b, htmlgo.Tag("turbo-stream"))
	return &b
}

type streamBuilder struct {
	*htmlgo_contrib.GenericBuilder[streamBuilder]
}

type Streams []streamBuilder

func (ts *streamBuilder) Action(action StreamAction) *streamBuilder {
	ts.Tag().Action(string(action))
	return ts
}

type StreamAction string

const (
	StreamAppend  StreamAction = "append"
	StreamPrepend StreamAction = "prepend"
	StreamReplace StreamAction = "replace"
	StreamUpdate  StreamAction = "update"
	StreamRemove  StreamAction = "remove"
	StreamBefore  StreamAction = "before"
	StreamAfter   StreamAction = "after"
)

func (ts *streamBuilder) Target(targetSelector string) *streamBuilder {
	ts.Tag().Attr("target", targetSelector)
	return ts
}

func (ts *streamBuilder) Template(children ...HTMLComponent) *streamBuilder {
	ts.Tag().Children(Template(children...))
	return ts
}

func (ts *streamBuilder) Render(ctxt http.ResponseWriter, ctx context.Context) {
	ctxt.Header().Add("Content-Type", "text/vnd.turbo-stream.html")
	ctxt.WriteHeader(200)

	htmlgo.Fprint(ctxt, ts.Tag(), ctx)
}

func (tss Streams) Render(ctxt http.ResponseWriter, ctx context.Context) {
	ctxt.Header().Add("Content-Type", "text/vnd.turbo-stream.html")
	ctxt.WriteHeader(200)

	for _, ts := range tss {
		htmlgo.Fprint(ctxt, ts.Tag(), ctx)
	}
}

type enhancedHTMLTagBuilder struct {
	*HTMLTagBuilder
}

func Extend(tag *HTMLTagBuilder) *enhancedHTMLTagBuilder {
	return &enhancedHTMLTagBuilder{tag}
}

func (b *enhancedHTMLTagBuilder) TurboDisabled(disabled bool) *enhancedHTMLTagBuilder {
	if !disabled {
		b.Data("turbo", "false")
	}
	return b
}

func (b *enhancedHTMLTagBuilder) TurboTrack(track bool) *enhancedHTMLTagBuilder {
	if track {
		b.Data("turbo-track", "reload")
	}

	return b
}

func (b *enhancedHTMLTagBuilder) TurboFrame(frameId string) *enhancedHTMLTagBuilder {
	b.Data("turbo-frame", frameId)
	return b
}

func (b *enhancedHTMLTagBuilder) TurboAction(action TurboVisitAction) *enhancedHTMLTagBuilder {
	b.Data("turbo-action", string(action))

	return b
}

func (b *enhancedHTMLTagBuilder) TurboPermanent(perm bool) *enhancedHTMLTagBuilder {
	if perm {
		b.Data("turbo-permanent")
	}
	return b
}

func (b *enhancedHTMLTagBuilder) TurboDisableCache(disable bool) *enhancedHTMLTagBuilder {
	if disable {
		b.Data("turbo-cache", "false")
	}

	return b
}

func (b *enhancedHTMLTagBuilder) TurboEval(disable bool) *enhancedHTMLTagBuilder {
	if disable {
		b.Data("turbo-eval", "false")
	}

	return b
}

func (b *enhancedHTMLTagBuilder) TurboMethod(method string) *enhancedHTMLTagBuilder {
	b.Data("turbo-method", method)
	return b
}

func (b *enhancedHTMLTagBuilder) TurboConfirm(text string) *enhancedHTMLTagBuilder {
	b.Data("turbo-confirm", text)
	return b
}
