package resources

import (
	"bytes"
	"embed"
	"fmt"
	"io/fs"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"path"
	"runtime"
	"testing"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/pbedat/golang/lang"
)

//go:embed all:example/scss1
var testFs embed.FS

func init() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
}

func TestAll(t *testing.T) {

	router := http.NewServeMux()

	router.HandleFunc("/test.scss", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
		fmt.Fprintln(w, `@use "test2";
		ul li {color: green;}`)
	})
	router.HandleFunc("/test2.scss", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
		fmt.Fprintln(w, "body p { padding: 1rem; }")
	})

	go http.ListenAndServe(":12345", router)
	runtime.Gosched()

	var sass = `
	@use 'http://localhost:12345/test.scss';
	@use "fg";
	@import "example/scss1/colors";
div { a { color: red; } }`

	log.Println(os.Getwd())

	fs.WalkDir(testFs, ".", func(path string, d fs.DirEntry, err error) error {
		log.Println(path)
		return nil
	})

	lang.Must(fs.Stat(testFs, "example/scss1/colors.scss"))

	mw, err := SassMiddleware(func(opt *SassMiddlewareOptions) {
		opt.IncludeFS(testFs)
		opt.IncludePath(path.Join(lang.Must(os.Getwd()), "example/scss2"))
	})
	if err != nil {
		t.Error(err)
	}

	router = http.NewServeMux()
	h := mw(router)

	router.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
		fmt.Fprint(w, Resources(r.Context()).Sass(sass).Href())
	})

	w := httptest.NewRecorder()

	body := bytes.NewBuffer([]byte{})
	h.ServeHTTP(
		w,
		httptest.NewRequest("GET", "/", body))

	if w.Result().StatusCode != 200 {
		t.Errorf("want status code 200, got %d", w.Result().StatusCode)
		t.FailNow()
	}

	css, err := ioutil.ReadAll(w.Result().Body)

	if err != nil {
		t.Error(err)
		t.FailNow()
	}

	w = httptest.NewRecorder()
	cssPath := string(css)
	h.ServeHTTP(w, httptest.NewRequest("", cssPath, nil))

	got := string(lang.Must(ioutil.ReadAll(w.Result().Body)))

	want := `body p{padding:1rem}ul li{color:green}body{color:red}h1{color:#00f}h1{size:2rem}body{background-color:#7fffd4}div a{color:red}
`
	if !cmp.Equal(got, want) {
		t.Error(cmp.Diff(got, want))
	}
}
