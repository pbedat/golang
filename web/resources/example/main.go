package main

import (
	"embed"
	_ "embed"
	"fmt"
	"io/fs"
	"log"
	"net/http"
	"os"
	"path"

	"gitlab.com/pbedat/golang/lang"
	"gitlab.com/pbedat/golang/web/resources"
)

//go:embed all:scss1
var scss embed.FS

func main() {

	log.SetFlags(log.LstdFlags | log.Lshortfile)

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "text/html")
		w.WriteHeader(200)

		fmt.Fprintf(w, `<html><head><link href="%s" type="text/css" rel="stylesheet"/></head>`,
			resources.Resources(r.Context()).Sass(`
			@use 'https://unpkg.com/bootstrap@5.2.0/scss/bootstrap.scss';
			@use "fg";
			@use "colors";
		div { a { color: red; } }`).Href())
		fmt.Fprintln(w, "<body><div><a class='btn btn-primary'>hello</a></div></body></html>")
	})

	sassMw := lang.Must(resources.SassMiddleware(func(opt *resources.SassMiddlewareOptions) {
		opt.IncludedPaths = append(opt.IncludedPaths, path.Join(lang.Must(os.Getwd()), "scss2"))
		opt.IncludedFS = append(opt.IncludedFS, lang.Must(fs.Sub(scss, "scss1")))
	}))

	log.Println("listening to :8080")
	http.ListenAndServe(":8080", sassMw(http.DefaultServeMux))
}
