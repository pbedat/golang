package resources

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"path"

	"github.com/gregjones/httpcache"
	"github.com/gregjones/httpcache/diskcache"
)

type urlImportResolver struct {
	httpClient *http.Client
}

func newUrlImportResolver() *urlImportResolver {
	c := diskcache.New(".cache/sass-url- resolver")
	httpClient := http.Client{
		Transport: httpcache.NewTransport(c),
	}

	return &urlImportResolver{&httpClient}
}

func (r *urlImportResolver) CanonicalizeURL(importUrl string) (string, error) {
	u, err := url.Parse(importUrl)
	if err != nil {
		return "", fmt.Errorf("import url could not be parsed: %w", err)
	}

	if u.Scheme != "http" && u.Scheme != "https" {
		return "", nil
	}

	if path.Ext(u.Path) != ".scss" {
		u.Path = u.Path + ".scss"
	}

	resp, err := r.httpClient.Get(u.String())
	if err != nil {
		return "", fmt.Errorf("http request failed: %w", err)
	}

	if resp.StatusCode >= 400 {
		u.Path = path.Join(path.Dir(u.Path), "_"+path.Base(u.Path))
		resp, err = r.httpClient.Get(u.String())
		if err != nil {
			return "", fmt.Errorf("http request failed: %w", err)
		}
	}

	if resp.StatusCode >= 400 {
		return "", fmt.Errorf("unexpected status code: %d", resp.StatusCode)
	}

	return u.String(), nil
}

func (r *urlImportResolver) Load(canonicalizedUrl string) (string, error) {
	log.Println("fetching", canonicalizedUrl)
	resp, err := r.httpClient.Get(canonicalizedUrl)
	if err != nil {
		return "", err
	}

	if resp.StatusCode >= 400 {
		return "", fmt.Errorf("unexpected status code: %d", resp.StatusCode)
	}

	content, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("could not read http response body: %w", err)
	}

	return string(content), nil
}
