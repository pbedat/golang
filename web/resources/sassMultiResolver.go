package resources

import (
	"fmt"
	"log"
	"net/url"
	"strconv"

	"github.com/bep/godartsass"
	"gitlab.com/pbedat/golang/lang"
)

func makeMultiResolver(resolvers ...godartsass.ImportResolver) multiResolver {
	return multiResolver{resolvers}
}

type multiResolver struct {
	resolvers []godartsass.ImportResolver
}

func (mr multiResolver) CanonicalizeURL(importUrl string) (string, error) {

	var err error
	for i, r := range mr.resolvers {
		canonicalUrl, err := r.CanonicalizeURL(importUrl)
		if err != nil {
			log.Println(err)
			continue
		}
		if canonicalUrl == "" {
			continue
		}

		u := lang.Must(url.Parse(canonicalUrl))
		q := u.Query()
		q.Add("__resolver", fmt.Sprint(i))
		u.RawQuery = q.Encode()

		return u.String(), nil
	}

	return "", err
}

func (r multiResolver) Load(canonicalizedUrl string) (string, error) {
	u, err := url.Parse(canonicalizedUrl)
	if err != nil {
		return "", err
	}

	resolverIndexParam := u.Query().Get("__resolver")
	resolverIndex := lang.Must(strconv.Atoi(resolverIndexParam))
	q := u.Query()
	q.Del("__resolver")
	u.RawQuery = q.Encode()

	return r.resolvers[resolverIndex].Load(u.String())
}
