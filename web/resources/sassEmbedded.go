package resources

import (
	"archive/tar"
	"compress/gzip"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"path"
	"runtime"

	"gitlab.com/pbedat/golang/lang"
)

var sassEmbeddedPath = path.Join(lang.Must(os.Getwd()), ".bin/dart-sass-embedded")

func prepareSassBinary() error {
	log.Println(sassEmbeddedPath)
	if _, err := os.Stat(sassEmbeddedPath); !os.IsNotExist(err) {
		log.Println(sassEmbeddedPath, "already exists - no download required")
		return nil
	}

	osArch := fmt.Sprintf("%s/%s", runtime.GOOS, runtime.GOARCH)
	var platform string

	switch osArch {
	case "linux/amd64":
		platform = "linux-x64"
	case "darwin/arm64":
		platform = "macos-arm64"
	case "darwin/amd64":
		platform = "macos-x64"
	}

	resp, err := http.Get(fmt.Sprintf("https://github.com/sass/dart-sass-embedded/releases/download/1.54.8/sass_embedded-1.54.8-%s.tar.gz", platform))

	if err != nil {
		return fmt.Errorf("could not download embedded sass: %w", err)
	}
	if resp.StatusCode >= 400 {
		return fmt.Errorf("unexpected status code: %d", resp.StatusCode)
	}

	if err := os.MkdirAll(path.Dir(sassEmbeddedPath), 0777); err != nil {
		return err
	}

	f, err := os.OpenFile(sassEmbeddedPath, os.O_WRONLY|os.O_TRUNC|os.O_CREATE, 0777)
	if err != nil {
		return fmt.Errorf("could not prepare download file: %w", err)
	}

	defer f.Close()

	log.Println("downloading dart-sass...")

	zr := lang.Must(gzip.NewReader(resp.Body))
	tr := tar.NewReader(zr)

	for {
		h, err := tr.Next()
		if err != nil {
			return err
		}

		if h.Typeflag != tar.TypeReg || h.Name != "sass_embedded/dart-sass-embedded" {
			continue
		}

		if _, err := io.Copy(f, tr); err != nil {
			f.Close()
			return fmt.Errorf("could not download file: %w", err)
		}
		break
	}

	return nil
}
