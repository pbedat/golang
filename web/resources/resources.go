package resources

import (
	"bytes"
	"context"
	"crypto/sha1"
	"encoding/base64"
	"fmt"
	"io"
	"io/fs"
	"log"
	"net/http"
	"sync"
	"time"

	"github.com/bep/godartsass"
	"github.com/evanw/esbuild/pkg/api"
	"github.com/gorilla/mux"
	"github.com/samber/lo"
)

type SassMiddlewareOptions struct {
	IncludedPaths []string
	IncludedFS    []fs.FS
}

func init() {
	log.SetFlags(0)
}

func (opts *SassMiddlewareOptions) IncludePath(paths ...string) {
	opts.IncludedPaths = append(opts.IncludedPaths, paths...)
}

func (opts *SassMiddlewareOptions) IncludeFS(fs ...fs.FS) {
	opts.IncludedFS = append(opts.IncludedFS, fs...)
}

func (opts *SassMiddlewareOptions) SetLogger(output io.Writer) {
	log.SetOutput(output)
	//opts.Logger = output
}

func SassMiddleware(options ...func(opt *SassMiddlewareOptions)) (mux.MiddlewareFunc, error) {
	option := &SassMiddlewareOptions{}
	lo.ForEach(options, func(configurer func(opt *SassMiddlewareOptions), _ int) {
		configurer(option)
	})

	if err := prepareSassBinary(); err != nil {
		return nil, fmt.Errorf("failed to prepare sass binary: %w", err)
	}

	log.Println("starting sass...")
	sass, err := godartsass.Start(godartsass.Options{
		Timeout:                  time.Minute * 2,
		DartSassEmbeddedFilename: sassEmbeddedPath,
		LogEventHandler: func(le godartsass.LogEvent) {
			log.Println(le.Message)
		},
	})
	if err != nil {
		return nil, err
	}
	log.Println("sass started")

	return mux.MiddlewareFunc(func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			r = r.WithContext(context.WithValue(r.Context(), resourcesKey, resourcesContext{transpiler: sass}))

			sassCacheMutex.RLock()
			css, ok := sassCache[r.URL.Path]
			sassCacheMutex.RUnlock()
			if ok {
				w.Header().Add("Content-Type", "text/css")
				w.WriteHeader(200)
				if _, err := w.Write([]byte(css)); err != nil {
					log.Println(err)
				}
				return
			}

			resourcesMutex.RLock()
			sassContent, ok := resources[r.URL.Path]
			resourcesMutex.RUnlock()
			if ok {
				css, err := sass.Execute(godartsass.Args{
					Source:       sassContent,
					IncludePaths: option.IncludedPaths,
					ImportResolver: makeMultiResolver(
						fsImportResolver{option.IncludedFS},
						newUrlImportResolver(),
					),
				})
				if err != nil {
					log.Println("could not transform sass:", err)
					w.WriteHeader(500)
					return
				}

				res := api.Transform(css.CSS, api.TransformOptions{
					Loader:            api.LoaderCSS,
					MinifyWhitespace:  true,
					MinifyIdentifiers: true,
					MinifySyntax:      true,
				})

				if len(res.Errors) > 0 {
					log.Println(res.Errors)
					w.WriteHeader(500)
					return
				}

				if len(res.Warnings) > 0 {
					log.Println(res.Warnings)
				}

				sassCacheMutex.Lock()
				sassCache[r.URL.Path] = string(res.Code)
				sassCacheMutex.Unlock()

				w.Header().Add("Content-Type", "text/css")
				w.WriteHeader(200)
				w.Write(res.Code)
				return
			}

			next.ServeHTTP(w, r)
		})
	}), nil
}

var resourcesKey = resourcesContext{}

type resourcesContext struct {
	transpiler *godartsass.Transpiler
}

type ResourcesBuilder struct {
	resourcesContext
}

func Resources(ctx context.Context) *ResourcesBuilder {
	return &ResourcesBuilder{ctx.Value(resourcesKey).(resourcesContext)}
}

// maps a file to sass content
var resources = make(map[string]string)
var resourcesMutex = &sync.RWMutex{}

// maps file to compiled css
var sassCache = make(map[string]string)
var sassCacheMutex = &sync.RWMutex{}

func (r *ResourcesBuilder) Sass(sass string) sassResourcesBuilder {
	h := sha1.New()
	fmt.Fprint(h, sass)

	hash := h.Sum(nil)

	b := bytes.NewBuffer([]byte{})

	if _, err := base64.NewEncoder(base64.RawURLEncoding, b).Write(hash); err != nil {
		panic(err)
	}

	path := fmt.Sprintf("/public/%s.css", b.String())
	resourcesMutex.Lock()
	resources[path] = sass
	resourcesMutex.Unlock()

	return sassResourcesBuilder{path}
}

type sassResourcesBuilder struct {
	path string
}

func (sass sassResourcesBuilder) Href() string {
	return sass.path
}

func (sass sassResourcesBuilder) Sring() string {
	return sass.Href()
}
