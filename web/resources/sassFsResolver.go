package resources

import (
	"errors"
	"fmt"
	"io/fs"
	"log"
	"net/url"
	"path"
	"strconv"
	"strings"

	"gitlab.com/pbedat/golang/lang"
)

type fsImportResolver struct {
	fs []fs.FS
}

func (r fsImportResolver) CanonicalizeURL(importUrl string) (string, error) {

	u, err := url.Parse(importUrl)
	if err != nil {
		return "", fmt.Errorf("could not parse import url: %w", err)
	}

	if path.Ext(u.Path) != ".scss" {
		u.Path = u.Path + ".scss"
	}

	if u.Scheme != "" && u.Scheme != "file" {
		return "", nil
	}

	u.Scheme = "file"

	for i, _fs := range r.fs {
		ok, p, err := findScssFile(_fs, u.Path)

		if err != nil {
			log.Println(err)
			continue
		}
		if ok {
			u.Path = p
			q := u.Query()
			q.Add("fs", fmt.Sprint(i))
			u.RawQuery = q.Encode()
			if !strings.HasPrefix(u.Path, "/") {
				u.Path = "/" + p
			}
			return u.String(), nil
		}
	}

	return "", nil
}

func (i fsImportResolver) Load(canonicalizedUrl string) (string, error) {

	u := lang.Must(url.Parse(canonicalizedUrl))

	fsIndex := lang.Must(strconv.ParseInt(u.Query().Get("fs"), 10, 32))
	path := strings.TrimPrefix(u.Path, "/")

	_fs := i.fs[fsIndex]

	b, err := fs.ReadFile(_fs, path)
	if err != nil {
		return "", err
	}

	return string(b), nil
}

func findScssFile(_fs fs.FS, filePath string) (bool, string, error) {
	filePath = strings.TrimPrefix(filePath, "/")

	_, err := fs.Stat(_fs, filePath)

	if err == nil {
		return true, filePath, nil
	}

	if !errors.Is(err, fs.ErrNotExist) {
		return false, "", err
	}

	filePath = path.Join(path.Dir(filePath), "_"+path.Base(filePath))

	_, err = fs.Stat(_fs, filePath)

	if err == nil {
		return true, filePath, nil
	}

	if !errors.Is(err, fs.ErrNotExist) {
		return false, "", err
	}

	return false, "", nil
}
