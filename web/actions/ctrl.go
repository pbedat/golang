package actions

import (
	"github.com/gorilla/mux"
)

type CtrlBuilder struct {
	middlewares []mux.MiddlewareFunc
	routeFn     func(r *mux.Router) *mux.Router
	pathPrefix  string
	name        string
	actions     []*Action
}

func Controller() *CtrlBuilder {
	return &CtrlBuilder{}
}

func (c *CtrlBuilder) Use(middlewares ...mux.MiddlewareFunc) *CtrlBuilder {
	c.middlewares = middlewares
	return c
}

func (c *CtrlBuilder) Router(routeFn func(r *mux.Router) *mux.Router) *CtrlBuilder {
	c.routeFn = routeFn
	return c
}

func (c *CtrlBuilder) PathPrefix(prefix string) *CtrlBuilder {
	c.pathPrefix = prefix
	return c
}

func (c *CtrlBuilder) Name(name string) *CtrlBuilder {
	c.name = name
	return c
}

func (c *CtrlBuilder) Actions(actions ...*Action) *CtrlBuilder {
	c.actions = actions
	return c
}

func (c *CtrlBuilder) Register(r *mux.Router) {
	if c.pathPrefix != "" {
		r = r.PathPrefix(c.pathPrefix).Subrouter()
	}
	if c.routeFn != nil {
		r = c.routeFn(r)
	}
	if len(c.middlewares) > 0 {
		r.Use(c.middlewares...)
	}

	for _, a := range c.actions {
		action := *a
		action.Builder.name = c.name
		action.Register(r)
	}
}
