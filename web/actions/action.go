package actions

import (
	"context"
	"fmt"
	"net/http"
	"strings"

	"github.com/gorilla/mux"
	"gitlab.com/pbedat/golang/contrib/gorilla"
)

type Builder struct {
	path    string
	name    string
	methods []string
	handler http.Handler
}

type Action struct {
	*Builder
}

func (a *Action) Name() string {
	return a.name
}

func Path(path string) *Builder {
	return &Builder{
		path: path,
	}
}

func (b *Builder) Name(name string) *Builder {
	b.name = name
	return b
}

func (b *Builder) Handler(h http.Handler) *Action {
	b.handler = h
	return &Action{b}
}

func (b *Builder) HandlerFunc(h http.HandlerFunc) *Action {
	b.handler = h
	return &Action{b}
}

func (b *Builder) Methods(methods ...string) *Builder {
	b.methods = methods
	return b
}

type Actions []Action

func (actions Actions) Register(r *mux.Router) {
	for _, a := range actions {
		a.Register(r)
	}
}

type Actionable interface {
	Register(r *mux.Router)
}

func (b *Builder) Register(r *mux.Router) {
	name := fmt.Sprint(b.name, strings.Join(b.methods, "_"), "_", b.path)
	b.name = name

	route := r.Path(b.path).Name(name)

	if len(b.methods) > 0 {
		route = route.Methods(b.methods...)
	}

	route.Handler(b.handler)
}

func (b *Action) Url(ctx context.Context, params ...interface{}) *UrlHelper {
	return &UrlHelper{gorilla.Url(ctx).Vars(params...), b.name}
}

type UrlHelper struct {
	*gorilla.UrlHelper
	name string
}

func (u *UrlHelper) Href() string {
	return u.UrlHelper.Href(u.name)
}
