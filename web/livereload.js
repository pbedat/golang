function handleBeforeUnload(ev) {
  ev.preventDefault();
  console.warn(
    "The web application is currently offline, navigation might fail"
  );
}

function livereload() {
  const es = new EventSource("{{ .baseUrl }}/_livereload/events");

  let online = false;

  es.addEventListener("open", () => {
    if(!online) {
      window.removeEventListener("beforeunload", handleBeforeUnload)
    }
    online = true;
  });

  es.addEventListener("error", () => {
    if (online) {
      window.addEventListener("beforeunload", handleBeforeUnload);
      online = false;
    }
  });

  es.addEventListener("change", () => window.location.reload());
}

livereload();
