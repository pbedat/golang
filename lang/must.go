package lang

import "reflect"

type mustl interface {
	any | *any
}

var errorInterface = reflect.TypeOf((*error)(nil)).Elem()

func Must[T mustl](t T, errVar ...error) T {
	if len(errVar) == 0 {
		tv := reflect.ValueOf(t)
		if tv.Kind() == reflect.Pointer && !tv.IsNil() && reflect.ValueOf(t).Type().Implements(errorInterface) {
			panic(t)
		} else {
			return t
		}
	}

	if len(errVar) > 0 {
		err := errVar[0]

		if err != nil {
			panic(err)
		}
	}

	return t
}
