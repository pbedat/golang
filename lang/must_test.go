package lang

import (
	"fmt"
	"testing"
)

func TestMust(t *testing.T) {

	t.Run("single error", func(t *testing.T) {

		defer func() {
			p := recover()
			t.Log(p)
			if p == nil {
				t.Error("should panic")
			}
		}()

		Must(foo(true))
	})

	t.Run("single nil", func(t *testing.T) {
		Must(foo(false))

	})

	t.Run("multi error", func(t *testing.T) {
		defer func() {
			p := recover()
			t.Log(p)
			if p == nil {
				t.Error("should panic")
			}
		}()

		Must(bar(true))
	})

	t.Run("multi success", func(t *testing.T) {
		wat := Must(bar(false))

		if wat != "wat" {
			t.Error("expected 'wat', got", wat)
		}
	})

}

func foo(throw bool) error {
	if throw {
		return fmt.Errorf("test")
	}
	return nil
}

func bar(throw bool) (string, error) {
	if throw {
		return "", fmt.Errorf("test")
	}
	return "wat", nil
}
