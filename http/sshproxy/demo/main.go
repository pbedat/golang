package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/user"
	"path"

	"gitlab.com/pbedat/golang/http/sshproxy"
)

var privKeyPath string
var userName string

func init() {

	homeDir := "~"

	homeDir, err := os.UserHomeDir()

	flag.StringVar(
		&privKeyPath,
		"privateKeyPath",
		path.Join(homeDir, ".ssh/id_rsa"),
		"path private key file in PEM format, e.g. /home/user/.ssh/id_rsa")

	flag.Parse()

	if err != nil && privKeyPath == "" {
		fmt.Println("current user has no home dir, cannot use default private key file path")
		fmt.Println("please specify --privateKeyPath")
		os.Exit(1)
	}

	user, err := user.Current()

	if err != nil {
		panic(err)
	}

	userName = user.Username
}

func main() {

	c := sshproxy.SshProxyConfig{
		PrivKeyPath: privKeyPath,
		Host:        "copilot.events",
	}
	proxy, err := sshproxy.Dial(c)

	if err != nil {
		panic(err)
	}

	http.Handle("alertmanager.misc.copilot.events/", proxy.Handle("localhost:9093"))

	http.ListenAndServe("127.0.0.8:8080", nil)
}
