package sshproxy

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"net/url"
	"os/exec"
	"os/user"
	"strings"
	"time"

	"golang.org/x/crypto/ssh"
)

type SshProxy struct {
	client *ssh.Client
}

type SshProxyConfig struct {
	// ssh server host
	Host string
	// ssh server port, defaults to 22 (optional)
	Port int
	// user to authenticate against at the ssh server
	User string
	// path to the private key, defaults to the default private of the current user (optional)
	PrivKeyPath string
	// path to the public key of the server, using the known_hosts by default (optional)
	PubKeyPath string
	// ignores the public key validation, default = false (optional)
	AllowInsecureHost bool
}

type parsedSshProxyConfig struct {
	privateKey      ssh.Signer
	hostKeyCallback ssh.HostKeyCallback
	SshProxyConfig
}

func (c SshProxyConfig) parseSshProxyConfig() (parsedSshProxyConfig, error) {
	privateKey, err := readPrivateKey(c.PrivKeyPath)

	if err != nil {
		return parsedSshProxyConfig{}, fmt.Errorf("config error: %w", err)
	}

	hostKeyCallback, err := readHostKeyCallback(c.Host, c.PubKeyPath, c.AllowInsecureHost)

	if err != nil {
		return parsedSshProxyConfig{}, fmt.Errorf("config error: %w", err)
	}

	parsedConfig := parsedSshProxyConfig{
		privateKey,
		hostKeyCallback,
		c,
	}

	if parsedConfig.User == "" {
		user, err := user.Current()

		if err != nil {
			return parsedSshProxyConfig{}, fmt.Errorf("config provided no username and fallback to the current user failed: %w", err)
		}

		parsedConfig.User = user.Username
	}

	if c.Port == 0 {
		parsedConfig.Port = 22 // ssh default port
	}

	return parsedConfig, nil
}

func readHostKeyCallback(host string, pubKeyPath string, ignoreHostValidation bool) (ssh.HostKeyCallback, error) {

	if ignoreHostValidation {
		return ssh.InsecureIgnoreHostKey(), nil
	}

	if pubKeyPath != "" {
		pubKeyBuf, err := ioutil.ReadFile(pubKeyPath)

		if err != nil {
			return nil, fmt.Errorf("unable to read public key: %w", err)
		}

		pubKey, err := ssh.ParsePublicKey(pubKeyBuf)

		if err != nil {
			return nil, err
		}

		return ssh.FixedHostKey(pubKey), nil
	}

	// fallback to ssh-keygen

	keygen := exec.Command("ssh-keygen", "-F", host)
	buf := bytes.NewBuffer([]byte{})
	keygen.Stdout = buf

	err := keygen.Run()

	if err != nil {
		return nil, fmt.Errorf("can't read known hosts: %w", err)
	}

	hostsBuf := buf.Bytes()

	if err != nil {
		return nil, fmt.Errorf("can't read known hosts file: %w", err)
	}

	_, _, pubKey, _, _, err := ssh.ParseKnownHosts(hostsBuf)

	if errors.Is(err, io.EOF) {
		return nil, fmt.Errorf("host key not found in known hosts, please add it manually with ssh-keyscan")
	}

	return ssh.FixedHostKey(pubKey), nil
}

func readPrivateKey(path string) (ssh.Signer, error) {
	pemBuf, err := ioutil.ReadFile(path)

	if err != nil {
		return nil, fmt.Errorf("can't read private key file: %w", err)
	}

	return ssh.ParsePrivateKey(pemBuf)
}

// Sets up a ssh connection to the serverAddr and provides handlers
// that forward http traffic to local addresses on the remote server
func Dial(config SshProxyConfig) (*SshProxy, error) {

	parsedConfig, err := config.parseSshProxyConfig()

	if err != nil {
		return nil, err
	}

	client, err := ssh.Dial("tcp", fmt.Sprintf("%s:%d", parsedConfig.Host, parsedConfig.Port), &ssh.ClientConfig{
		User: parsedConfig.User,
		Auth: []ssh.AuthMethod{
			ssh.PublicKeys(
				parsedConfig.privateKey,
			),
		},
		HostKeyCallback: parsedConfig.hostKeyCallback,
	})

	if err != nil {
		return nil, err
	}

	return &SshProxy{
		client,
	}, nil
}

// Creates a http.Handler, that forwards traffic to the remoteAddr on the remote server
func (p *SshProxy) Handle(remoteAddr string) http.Handler {

	upstreamClient := &http.Client{
		Transport: &http.Transport{
			DialContext: func(ctx context.Context, network, addr string) (net.Conn, error) {
				return p.client.Dial(network, addr)
			},
			ForceAttemptHTTP2:     true,
			MaxIdleConns:          100,
			IdleConnTimeout:       90 * time.Second,
			TLSHandshakeTimeout:   10 * time.Second,
			ExpectContinueTimeout: 1 * time.Second,
		},
	}

	return &proxyHandler{
		remoteAddr,
		upstreamClient,
	}
}

type proxyHandler struct {
	remoteAddr     string
	upstreamClient *http.Client
}

func (h proxyHandler) ServeHTTP(w http.ResponseWriter, req *http.Request) {

	forwardUrl, _ := url.Parse(req.URL.String())
	forwardUrl.Host = h.remoteAddr
	forwardUrl.Scheme = "http"

	forwardReq, err := http.NewRequest(req.Method, forwardUrl.String(), req.Body)

	if err != nil {
		log.Println("failed to create proxy request", err)
		http.Error(w, "failed to proxy request", 500)
		return
	}

	res, err := h.upstreamClient.Do(forwardReq)

	if err != nil {
		log.Println("failed to proxy request", err)
		http.Error(w, "Cannot proxy request", http.StatusBadGateway)
		return
	}

	for h, v := range res.Header {
		w.Header().Add(h, strings.Join(v, ","))
	}

	w.WriteHeader(res.StatusCode)

	_, err = io.Copy(w, res.Body)

	if err != nil {
		log.Println("failed to write response body", err)
	}
}
